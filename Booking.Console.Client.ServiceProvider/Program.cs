﻿using System;
using System.Threading.Tasks;
using Booking.Console.Client.ServiceProvider.Messages;
using Booking.Contracts.BookService.Commands;
using Booking.Contracts.BookService.Events;
using Booking.Contracts.Configuration;
using Booking.Contracts.CustomerService.Request;
using Booking.Contracts.CustomerService.Response;
using MassTransit;

namespace Booking.Console.Client.ServiceProvider
{
    class Program
    {
        private static IBusControl _busControl;

        private static Guid _interestedCustomerGuid;

        static void Main(string[] args)
        {
            System.Console.Title = "ServiceProvider Client";

            _busControl = ConfigureBus();
            _busControl.Start();

            string input = String.Empty;
            while (input != "exit")
            {
                input = System.Console.ReadLine();

                if (input == "exit")
                {
                    break;
                }
                else if (input == "send")
                {
                    var id = System.Console.ReadLine();

                    Uri address = new Uri($"{RabbitMqConstants.RabbitMqUri}{RabbitMqConstants.CustomerService}");
                    TimeSpan requestTimeout = TimeSpan.FromSeconds(30);

                    IRequestClient<IGetInterestedCustomer, IGetInterestedCustomerResponse> client =
                        new MessageRequestClient<IGetInterestedCustomer, IGetInterestedCustomerResponse>(_busControl, address, requestTimeout);

                    if (id != null)
                    {
                        var response = client.Request(new GetInterestedCustomer { IntrestedCustomerId = new Guid(id)}).Result;

                        var sendUri = new Uri($"{RabbitMqConstants.RabbitMqUri}{RabbitMqConstants.BookService_BookReservation}");
                        var endPoint = _busControl.GetSendEndpoint(sendUri).Result;

                        var offer = new SendOfferCommand
                        {
                            CorrelationId = _interestedCustomerGuid,
                            Timestamp = DateTime.UtcNow,
                            Message = "Witam obecnie w ofercie mamy rowniez znizki na nasze uslugi",
                            Cost = 100.00m,
                            CustomerId = response.CustomerId,
                            ServiceId = Guid.NewGuid(), // trezba pobrac z bazy danych
                            Start = DateTime.UtcNow.AddDays(10),
                            End = DateTime.UtcNow.AddDays(10),
                            ServiceName = "Moja zajefajna sala",
                            ServiceProviderId = Guid.NewGuid()
                        };

                        endPoint.Send<ISendOfferCommand>(offer);
                    }
                }
            }
   
            _busControl?.Stop();
        }


        static IBusControl ConfigureBus()
        {
            return Bus.Factory.CreateUsingRabbitMq(cfg =>
            {
                var host = cfg.Host(new Uri("rabbitmq://localhost/booking"), h =>
                {
                    h.Username("guest");
                    h.Password("guest");
                });

                cfg.ReceiveEndpoint(host, RabbitMqConstants.ServiceProviderClient, e =>
                {
                    e.Handler<ICustomerInterestedEvent>(context =>
                    {
                        _interestedCustomerGuid = context.Message.CorrelationId;
                        return
                            System.Console.Out.WriteLineAsync(
                                $"Klient {context.Message.CustomerId} jest zainteresowany twoim serwisem {context.Message.ServiceName}" +
                                $"w dniu {context.Message.Start} - {context.Message.End}");
                    }
                    );

                    e.Handler<IReservationVisitedEvent>(context => System.Console.Out.WriteLineAsync(
                        $"Ten termin jest bardzo oblegany {context.Message.ServiceId}. Ilosc wejsc: {context.Message.Count}"));
                });
            });
        }
    }
}
