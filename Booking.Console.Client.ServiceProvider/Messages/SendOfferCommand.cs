﻿using System;
using Booking.Contracts.BookService.Commands;

namespace Booking.Console.Client.ServiceProvider.Messages
{
    public class SendOfferCommand : ISendOfferCommand
    {
        public Guid CustomerId { get; set; }
        public Guid ServiceProviderId { get; set; }
        public Guid ServiceId { get; set; }
        public string ServiceName { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string Message { get; set; }
        public decimal Cost { get; set; }
        public Guid CorrelationId { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
