﻿using System;
using Booking.Contracts.CustomerService.Request;

namespace Booking.Console.Client.ServiceProvider.Messages
{
    public class GetInterestedCustomer : IGetInterestedCustomer
    {
        public Guid IntrestedCustomerId { get; set; }
    }
}
