﻿using System;
using MassTransit;

namespace Booking.Web.Models
{
    public abstract class BaseMessage : CorrelatedBy<Guid>
    {
        protected BaseMessage()
        {
            CorrelationId = Guid.NewGuid();
        }

        protected BaseMessage(Guid correlationId)
        {
            CorrelationId = correlationId;
        }

        public Guid CorrelationId { get; set; }
    }
}
