﻿using System;

namespace Booking.Web.Models
{
    public class Customer : BaseMessage
    {
        public Guid CustomerId { get; set; }
        public string FirstName { get; set; }
        public string Lastname { get; set; }
        public DateTime BirthDay { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
    }
}
