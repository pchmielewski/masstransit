﻿using System;
using Booking.Contracts.BookService.Commands;

namespace Booking.Web.Models.Booking
{
    public class CustomerVisitReservationCommandDTO : ICustomerVisitReservationCommand
    {
        public Guid ServiceProviderId { get; set; }
        public Guid ServiceId { get; set; }
        public string ServiceName { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
    }
}
