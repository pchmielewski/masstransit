﻿using System;
using Booking.Contracts.BookService.Commands;

namespace Booking.Web.Models.Booking
{
    public class CustomerConsiderReservationDTO : BaseMessage, ICustomerConsiderReservationCommand
    {
        public DateTime Timestamp { get; set; }
        public Guid CustomerId { get; set; }
        public Guid ServiceProviderId { get; set; }
        public Guid ServiceId { get; set; }
        public string ServiceName { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
    }
}
