﻿using MassTransit.EntityFrameworkIntegration;

namespace Booking.CustomerService.StateMachines.RegisterCustomer
{
    public class RegisterCustomerMap : SagaClassMapping<RegisterCustomerState>
    {
        public RegisterCustomerMap()
        {
            Property(x => x.FirstName);
            Property(x => x.BirthDay);
            Property(x => x.City);
            Property(x => x.Country);
            Property(x => x.CurrentState).HasMaxLength(64);
            Property(x => x.CustomerId);
            Property(x => x.Lastname);
        }
    }
}
