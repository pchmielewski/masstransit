﻿using System;
using Core.Service.StateMachine;

namespace Booking.CustomerService.StateMachines.RegisterCustomer
{
    public class RegisterCustomerState : IBaseSagaStateMachine
    {
        public Guid CorrelationId { get; set; }
        public string CurrentState { get; set; }

        public Guid CustomerId { get; set; }
        public string FirstName { get; set; }
        public string Lastname { get; set; }
        public DateTime BirthDay { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
    }
}
