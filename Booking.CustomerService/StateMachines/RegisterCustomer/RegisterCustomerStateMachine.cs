﻿using System;
using System.Threading.Tasks;
using Automatonymous;
using Booking.Contracts.CustomerService.Commands;
using Booking.Contracts.CustomerService.Events;
using Booking.CustomerService.Messages.Events;

namespace Booking.CustomerService.StateMachines.RegisterCustomer
{
    public class RegisterCustomerStateMachine : MassTransitStateMachine<RegisterCustomerState>
    {
        //states
        public State Received { get; private set; }
        public State Registered { get; private set; }
        public State Updated { get; private set; }


        //commands
        public Event<IRegisterCustomerCommand> RegisterCustomer { get; private set; }
        public Event<IUpdateCustomerCommand> UpdateCustomer { get; private set; }


        //events
        public Event<ICustomerRegisteredEvent> CustomerRegistered { get; private set; }

        public RegisterCustomerStateMachine()
        {
            InstanceState(s => s.CurrentState);

            Event(()=>RegisterCustomer, x=>x.CorrelateById(context=>context.Message.CorrelationId)); // zamienic na customer ID
            Event(()=> UpdateCustomer, x=>x.CorrelateById(context=>context.Message.CorrelationId));
            Event(()=> CustomerRegistered, x=>x.CorrelateById(context=>context.Message.CorrelationId));

            Initially(

                When(RegisterCustomer) // powyciagac do metod
                    .Then(context =>
                    {
                        context.Instance.CorrelationId = context.Data.CorrelationId;
                        context.Instance.CustomerId = context.Data.CustomerId;
                        context.Instance.BirthDay = context.Data.BirthDay;
                        context.Instance.FirstName = context.Data.FirstName;
                        context.Instance.Lastname = context.Data.Lastname;
                        context.Instance.City = context.Data.City;
                        context.Instance.Country = context.Data.Country;
                    })
                    .ThenAsync(context => Console.Out.WriteLineAsync($"Customer added {context.Data.FirstName} {context.Data.Lastname}.CorrelationId {context.Data.CorrelationId}"))
                    .TransitionTo(Received)
                    .Publish(context => new CustomerReceivedEvent
                    {
                        CorrelationId = context.Instance.CorrelationId,
                        CustomerId = context.Instance.CustomerId,
                        BirthDay = context.Instance.BirthDay,
                        FirstName  = context.Instance.FirstName,
                        Lastname = context.Instance.Lastname,
                        City = context.Instance.City,
                        Country =  context.Instance.Country 
                        
                    }),

                When(CustomerRegistered)
                    .ThenAsync(context=>StateMachineLogicError()),

                When(UpdateCustomer)
                    .ThenAsync(context => StateMachineLogicError())
            );

            During(Received,
                When(CustomerRegistered)
                    .ThenAsync(context => Console.Out.WriteLineAsync($"Customer Registred {context.Data.FirstName} {context.Data.Lastname}.CorrelationId {context.Data.CorrelationId}"))
                    .TransitionTo(Registered)
            );

            During(Registered,
                When(UpdateCustomer)
                    .Then(context =>
                    {
                        context.Instance.CorrelationId = context.Data.CorrelationId;
                        context.Instance.CustomerId = context.Data.CustomerId;
                        context.Instance.BirthDay = context.Data.BirthDay;
                        context.Instance.FirstName = context.Data.FirstName;
                        context.Instance.Lastname = context.Data.Lastname;
                        context.Instance.City = context.Data.City;
                        context.Instance.Country = context.Data.Country;
                    })
                    .ThenAsync(context => Console.Out.WriteLineAsync($"Customer request update of data {context.Data.FirstName} {context.Data.Lastname}.CorrelationId {context.Data.CorrelationId}"))
                    .Finalize()// zeby stany zostaly utrzymane nalezy nie uzywac tej metody
                    // dlatego mozemy nie wywolywac tego i zamiast tego stworzyc 
            );
              


            SetCompletedWhenFinalized();
        }

        private async Task StateMachineLogicError()
        {
            Console.Out.WriteLineAsync($"Blad maszyny stanowej, prawdopodobnie nie zostal przekazany CorrelationId");
        }
    }
}
