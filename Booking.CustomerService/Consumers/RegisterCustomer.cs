﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Booking.Contracts.CustomerService.Events;
using Booking.Contracts.CustomerService.Request;
using Booking.Contracts.CustomerService.Response;
using Booking.CustomerService.DAL;
using Booking.CustomerService.DAL.Entities;
using Booking.CustomerService.Messages.Events;
using Booking.CustomerService.Messages.Responses;
using MassTransit;

namespace Booking.CustomerService.Consumers
{
    public class RegisterCustomer : 
        IConsumer<ICustomerReceivedEvent>,
        IConsumer<IGetInterestedCustomer>
    {
        public async Task Consume(ConsumeContext<ICustomerReceivedEvent> context)
        {
            await Console.Out.WriteLineAsync($"RegisterCustomer handle customer {context.Message.FirstName} {context.Message.Lastname}.CorrelationId {context.Message.CorrelationId}");

            await Console.Out.WriteLineAsync($"RegisterCustomer handle customer. CustomerId {context.Message.CustomerId}");

            Customer customer;
            using (var dbContext = new CustomerContext())
            {
                customer = dbContext.Customers.Add(new Customer
                {
                    CustomerId = context.Message.CustomerId,
                    FirstName = context.Message.FirstName,
                    Lastname = context.Message.Lastname,
                    Country = context.Message.Country,
                    BirthDay = context.Message.BirthDay,
                    City = context.Message.City,
                });
                await dbContext.SaveChangesAsync();
            }
            await Console.Out.WriteLineAsync($"RegisterCustomer handle customer {customer.FirstName} {customer.Lastname}.CorrelationId {customer.CustomerId}");


            //publish event
            await context.Publish<ICustomerRegisteredEvent>(
                new CustomerRegisteredEvent
                {
                    CorrelationId = context.Message.CorrelationId,
                    CustomerId = context.Message.CustomerId,
                    FirstName = context.Message.FirstName,
                    Lastname = context.Message.Lastname,
                    Country = context.Message.Country,
                    BirthDay = context.Message.BirthDay,
                    City = context.Message.City,
                    Timestamp = DateTime.UtcNow,

                });
        }

        public async Task Consume(ConsumeContext<IGetInterestedCustomer> context)
        {
            await Console.Out.WriteLineAsync($"Uslugodawca wyslal zapytanie o dane klienta o id {context.Message.IntrestedCustomerId}");

            using (var dbContext = new CustomerContext())
            {
                var customer = dbContext.Customers.FirstOrDefault(p => p.CustomerId == context.Message.IntrestedCustomerId);

                if (customer == null)
                {
                    // trzeba wyslac wiadomosc ze nie ma takiego uzytkownika
                }
                else
                {
                    await context.RespondAsync<IGetInterestedCustomerResponse>(new GetInterestedCustomerResponse
                    {
                        CustomerId = customer.CustomerId,
                        FirstName = customer.FirstName,
                        Country = customer.Country,
                        Lastname = customer.Lastname,
                        BirthDay = customer.BirthDay,
                        City = customer.City
                    });

                    await Console.Out.WriteLineAsync($"Dane o uzytkowniku {customer.FirstName} {customer.Lastname} zostale wyslane");
                }
            }
        }
    }
}
