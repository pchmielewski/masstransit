﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Booking.Contracts.CustomerService.Events;
using MassTransit;

namespace Booking.CustomerService.Consumers.HandlingFaults
{
    public class CustomerRegisteredEventFaultConsumer : IConsumer<Fault<ICustomerRegisteredEvent>>
    {
        public async Task Consume(ConsumeContext<Fault<ICustomerRegisteredEvent>> context)
        {
            var exceptions = context.Message.Exceptions;
            await Console.Out.WriteLineAsync($"##### Exception: " + string.Join(",", exceptions.Select(p => p.Message)));

            await Console.Out.WriteLineAsync($"##### Exception: BirthDay:" + context.Message.Message.BirthDay + " Timestamp:" + context.Message.Message.Timestamp);
        }
    }
}
