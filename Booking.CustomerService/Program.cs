﻿using System;
using System.Configuration;
using Automatonymous;
using Booking.Contracts.Configuration;
using Booking.CustomerService.Consumers;
using Booking.CustomerService.Consumers.HandlingFaults;
using Booking.CustomerService.StateMachines.RegisterCustomer;
using Core.Service.Common;
using MassTransit;
using MassTransit.EntityFrameworkIntegration;
using MassTransit.EntityFrameworkIntegration.Saga;
using MassTransit.Saga;

namespace Booking.CustomerService
{
    class Program
    {
        private static IBusControl _bus;

        static Lazy<ISagaRepository<RegisterCustomerState>> _repository;

        static void Main(string[] args)
        {
            Console.Title = "Customer Service";

            var registerCustomerStateMachine = new RegisterCustomerStateMachine();

            SagaRepository();

            _bus = Bus.Factory.CreateUsingRabbitMq(cfg =>
            {
                var host = cfg.Host(new Uri(RabbitMqConstants.RabbitMqUri), h =>
                {
                    h.Username(RabbitMqConstants.UserName);
                    h.Password(RabbitMqConstants.Password);
                });

                cfg.ReceiveEndpoint(host, RabbitMqConstants.CustomerService + RabbitMqConstants.RegisterCustomerSaga, e =>
                {
                    e.StateMachineSaga(registerCustomerStateMachine, _repository.Value);
                    //e.PrefetchCount = 8;
                });

                cfg.ReceiveEndpoint(host, RabbitMqConstants.CustomerService, e =>
                {
                    e.Consumer<RegisterCustomer>();
                });

                cfg.ReceiveEndpoint(host, RabbitMqConstants.CustomerService + ".faults", e =>
                {
                    e.Consumer<RegisterCustomerFaults>();
                });

                cfg.ReceiveEndpoint(host, RabbitMqConstants.CustomerService + ".faults2", e =>
                {
                    e.Consumer<CustomerRegisteredEventFaultConsumer>();
                });


                cfg.UseRetry(Retry.Intervals(5, 5));

            });

            //AddObservers();

            _bus.Start();

            Console.WriteLine("Customer Service is running");


            Console.ReadLine();
            _bus?.Stop();
        }

        private static void SagaRepository()
        {
            if (true)
            {

                SagaDbContextFactory sagaDbContextFactory =
                    () =>
                        new SagaDbContext<RegisterCustomerState, RegisterCustomerMap>(
                            ConfigurationManager.ConnectionStrings["CustomerService"].ConnectionString);


                _repository = new Lazy<ISagaRepository<RegisterCustomerState>>(
                    () => new EntityFrameworkSagaRepository<RegisterCustomerState>(sagaDbContextFactory));
            }
            else
            {
                var registerCustomer = new InMemorySagaRepository<RegisterCustomerState>();
            }
        }

        private static void AddObservers()
        {
            var observer = new ReceiveObserver();
            var handle = _bus.ConnectReceiveObserver(observer);
        }
    }
}
