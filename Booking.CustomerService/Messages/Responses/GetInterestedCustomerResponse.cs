﻿using System;
using Booking.Contracts.CustomerService.Response;

namespace Booking.CustomerService.Messages.Responses
{
    public class GetInterestedCustomerResponse : IGetInterestedCustomerResponse
    {
        public Guid CustomerId { get; set; }
        public string FirstName { get; set; }
        public string Lastname { get; set; }
        public DateTime BirthDay { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
    }
}
