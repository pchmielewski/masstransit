﻿using System;
using Booking.Contracts.CustomerService.Events;

namespace Booking.CustomerService.Messages.Events
{
    public class CustomerReceivedEvent : ICustomerReceivedEvent
    {
        public Guid CorrelationId { get; set; }
        public DateTime Timestamp { get; set; }
        public Guid CustomerId { get; set; }
        public string FirstName { get; set; }
        public string Lastname { get; set; }
        public DateTime BirthDay { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
    }
}
