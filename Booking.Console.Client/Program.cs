﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Booking.Contracts.Configuration;
using Booking.Contracts.CustomerService.Commands;
using Booking.CustomerService.DAL;
using Booking.Web.Models;
using GenFu;
using MassTransit;

namespace Booking.Console.Client
{
    class Program
    {
        private static IBusControl _busControl;

        static void Main(string[] args)
        {
            System.Console.Title = "Client";

            A.MinDateTime = DateTime.Now.AddYears(-40);
            A.MaxDateTime = DateTime.Now.AddYears(40);

            var Name = A.ListOf<Customer>(1).First().FirstName;

            _busControl = ConfigureBus();
            _busControl.Start();
            System.Console.WriteLine("Press key to start");
            System.Console.ReadLine();

            var sendUri = new Uri($"{RabbitMqConstants.RabbitMqUri}{RabbitMqConstants.CustomerService}{RabbitMqConstants.RegisterCustomerSaga}");
            var endPoint =  _busControl.GetSendEndpoint(sendUri).Result;

            var message = new Customer
            {
                BirthDay = DateTime.UtcNow,
                City = "Częstochowa",
                Country = "Poland",
                CustomerId = Guid.NewGuid(),
                FirstName = "John",
                Lastname = "Locke"
            };

            var customers = A.ListOf<Customer>(100);

            Parallel.ForEach(customers, customer =>
            {
                customer.FirstName = Name;
                customer.CorrelationId = customer.CustomerId;
                endPoint.Send<IRegisterCustomerCommand>(customer);
                Thread.Sleep(1000);
            });

            System.Console.WriteLine("Eddit users");
            System.Console.ReadLine();

            List<Booking.CustomerService.DAL.Entities.Customer> toEdit = new List<CustomerService.DAL.Entities.Customer>();
            using (var dbContext = new CustomerContext())
            {

                foreach (var customer in customers)
                {
                    toEdit.Add(dbContext.Customers.First(p=>p.CustomerId == customer.CustomerId));
                }

                System.Console.WriteLine($"toEdit:{toEdit.First().CustomerId}");
            }

            foreach (var customer in toEdit)
            {
                var customerModel = new Customer
                {
                    FirstName = Name,
                    CorrelationId = customer.CustomerId,
                    CustomerId = customer.CustomerId,
                    BirthDay = customer.BirthDay,
                    //FirstName = customer.FirstName,
                    Lastname = customer.Lastname,
                    City = customer.City,
                    Country = customer.Country
                };
                System.Console.WriteLine($"toEdit:{customerModel.CustomerId} CorId:{customerModel.CustomerId}");
                endPoint.Send<IUpdateCustomerCommand>(customerModel);
            }


            System.Console.ReadLine();
            _busControl?.Stop();
        }


        static IBusControl ConfigureBus()
        {
            return Bus.Factory.CreateUsingRabbitMq(cfg =>
            {
                var host = cfg.Host(new Uri("rabbitmq://localhost/booking"), h =>
                {
                    h.Username("guest");
                    h.Password("guest");
                });
            });
        }
    }
}
