﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using Booking.BookService.DAL.Entities;

namespace Booking.BookService.DAL
{
    public class BookingContext : DbContext
    {
        public BookingContext() : base("name=BookService")
        {

        }

        public DbSet<InterestedCustomer> InterestedCustomers { get; set; } // czy jest w ogole potrzebna ??
        public DbSet<LoggedEvent> LogedEvents { get; set; }
        public DbSet<CustomerVisited> CustomerVisited { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("BookService");

            modelBuilder.Entity<InterestedCustomer>().HasKey(x => x.Id);
            modelBuilder.Entity<InterestedCustomer>().Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<LoggedEvent>().HasKey(x => x.Id);
            modelBuilder.Entity<LoggedEvent>().Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<CustomerVisited>().HasKey(x => x.Id);
            modelBuilder.Entity<CustomerVisited>().Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            base.OnModelCreating(modelBuilder);
        }
    }
}
