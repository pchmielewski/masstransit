﻿using System;

namespace Booking.BookService.DAL.Entities
{
    public class InterestedCustomer
    {
        public Guid Id { get; set; }
        public Guid CustomerId { get; set; }
        public Guid ServiceProviderId { get; set; }
        public Guid ServiceId { get; set; }

        public string ServiceName { get; set; }

        public DateTime Start { get; set; }
        public DateTime End { get; set; }
    }
}
