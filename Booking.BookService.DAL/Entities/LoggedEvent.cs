﻿using System;

namespace Booking.BookService.DAL.Entities
{
    public class LoggedEvent
    {
        public Guid Id { get; set; }
        public string Action { get; set; }
        public Guid AggregationId { get; set; }
        public string Messaage { get; set; }
    }
}
