﻿using Automatonymous;

namespace Core.Service.StateMachine
{
    public interface IBaseSagaStateMachine : SagaStateMachineInstance
    {
        string CurrentState { get; set; }
    }
}
