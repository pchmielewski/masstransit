﻿using System;
using System.Threading.Tasks;
using MassTransit;

namespace Core.Service.Common
{
    public class ReceiveObserver : IReceiveObserver
    {
        public async Task PreReceive(ReceiveContext context)
        {
            await Console.Out.WriteLineAsync("PreReceive of:" + context.InputAddress);
        }

        public async Task PostReceive(ReceiveContext context)
        {
            await Console.Out.WriteLineAsync("PostReceive of:" + context.InputAddress);
        }

        public async Task PostConsume<T>(ConsumeContext<T> context, TimeSpan duration, string consumerType) where T : class
        {
            await Console.Out.WriteLineAsync("PostConsume of:" + context.ConversationId);
        }

        public async Task ConsumeFault<T>(ConsumeContext<T> context, TimeSpan duration, string consumerType, Exception exception) where T : class
        {
            await Console.Out.WriteLineAsync("ConsumeFault of:" + context.ConversationId);
        }

        public async Task ReceiveFault(ReceiveContext context, Exception exception)
        {
            await Console.Out.WriteLineAsync("ReceiveFault of:" + context.InputAddress);
        }
    }
}
