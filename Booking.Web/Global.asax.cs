﻿using System;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using MassTransit;

namespace Booking.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private static IBusControl _busControl;

        public static IBus BusInstance => _busControl;

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            _busControl = ConfigureBus();
            _busControl.Start();
        }

        protected void Application_End()
        {
            _busControl?.Stop(TimeSpan.FromSeconds(10));
        }

        IBusControl ConfigureBus()
        {
            return Bus.Factory.CreateUsingRabbitMq(cfg =>
            {
                var host = cfg.Host(new Uri("rabbitmq://localhost/booking"), h =>
                {
                    h.Username("guest");
                    h.Password("guest");
                });
            });
        }
    }
}
