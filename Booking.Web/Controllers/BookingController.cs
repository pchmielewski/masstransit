﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Mvc;
using Booking.Contracts.BookService.Commands;
using Booking.Contracts.Configuration;
using Booking.Contracts.CustomerService.Commands;
using Booking.Web.Models.Booking;

namespace Booking.Web.Controllers
{
    public class BookingController : Controller
    {
        // GET: Booking
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Przegladanie terminu ktory mnie interesuje
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> Visited()
        {
            var sendUri = new Uri($"{RabbitMqConstants.RabbitMqUri}{RabbitMqConstants.CustomerVisitedQueue}");
            var endPoint = await MvcApplication.BusInstance.GetSendEndpoint(sendUri);

            var message = new CustomerVisitReservationCommandDTO
            {
                Start = DateTime.UtcNow.AddDays(3),
                End = DateTime.UtcNow.AddDays(6),
                ServiceId = Guid.NewGuid(), // id restauracji (uslugodawca moze miec kilka)
                ServiceName = "Restauracja Jujarska",
                ServiceProviderId = Guid.NewGuid() // id uslugodawcy
            };

            for (int i = 0; i < 10; i++)
            {
                await endPoint.Send<ICustomerVisitReservationCommand>(message);
                Thread.Sleep(1000); // tutaj jest problem ze baza danych jest wstanie tak szybko tego zapisac 
            }

            return View("Index");
        }


        /// <summary>
        /// Przegladanie terminu ktory mnie interesuje
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> Looking()
        {
            var sendUri = new Uri($"{RabbitMqConstants.RabbitMqUri}{RabbitMqConstants.BookService_BookReservation}");
            var endPoint = await MvcApplication.BusInstance.GetSendEndpoint(sendUri);

            var message = new CustomerConsiderReservationDTO
            {
                CustomerId = Guid.NewGuid(), //pobierac z bazy 
                Timestamp = DateTime.UtcNow,
                Start = DateTime.UtcNow.AddDays(3),
                End = DateTime.UtcNow.AddDays(6),
                ServiceId = Guid.NewGuid(), // id restauracji (uslugodawca moze miec kilka)
                ServiceName = "Restauracja Jujarska",
                ServiceProviderId = Guid.NewGuid() // id uslugodawcy
            };

            await endPoint.Send<ICustomerConsiderReservationCommand>(message);

            return View("Index");
        }
    }
}