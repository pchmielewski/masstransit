﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Booking.Contracts.Configuration;
using Booking.Contracts.CustomerService.Commands;
using Booking.CustomerService.DAL;
using Booking.Web.Models;
using MassTransit;

namespace Booking.Web.Controllers
{
    public class CustomerController : Controller
    {
        private static Guid correlationId;

        public ActionResult Index()
        {
            return View();
        }


        public async Task<ActionResult> AddUser()
        {
            var sendUri = new Uri($"{RabbitMqConstants.RabbitMqUri}{RabbitMqConstants.CustomerService}{RabbitMqConstants.RegisterCustomerSaga}");
            var endPoint =  await MvcApplication.BusInstance.GetSendEndpoint(sendUri);

            var message = new Customer
            {
                BirthDay = DateTime.UtcNow,
                City = "Częstochowa",
                Country = "Poland",
                FirstName = "John",
                Lastname = "Locke"
            };
            correlationId = message.CorrelationId;
            message.CustomerId = message.CorrelationId;

            await endPoint.Send<IRegisterCustomerCommand>(message);

            return View("Index");
        }

        public async Task<ActionResult> EditUser()
        {
            var sendUri = new Uri($"{RabbitMqConstants.RabbitMqUri}{RabbitMqConstants.CustomerService}{RabbitMqConstants.RegisterCustomerSaga}");
            var endPoint = await MvcApplication.BusInstance.GetSendEndpoint(sendUri);

            Booking.CustomerService.DAL.Entities.Customer customer;
            using (var dbContext = new CustomerContext())
            {
                customer = dbContext.Customers.First(p=>p.CustomerId == correlationId);
            }

            await endPoint.Send<IUpdateCustomerCommand>(new Customer
            {
                CorrelationId = customer.CustomerId,
                BirthDay = DateTime.UtcNow,
                City = "Wyspa :D",
                Country = "Poland",
                CustomerId = customer.CustomerId,
                FirstName = "John",
                Lastname = "Locke"
            });

            return View("Index");
        }
    }
}