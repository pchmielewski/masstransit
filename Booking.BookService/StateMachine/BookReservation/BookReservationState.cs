﻿using System;
using Core.Service.StateMachine;

namespace Booking.BookService.StateMachine.BookReservation
{
    public class BookReservationState : IBaseSagaStateMachine
    {
        public Guid CorrelationId { get; set; }
        public string CurrentState { get; set; }

        public Guid CustomerId { get; set; }
        public Guid ServiceProviderId { get; set; }
        public Guid ServiceId { get; set; }

        public string ServiceName { get; set; }

        public DateTime Start { get; set; }
        public DateTime End { get; set; }
    }
}
