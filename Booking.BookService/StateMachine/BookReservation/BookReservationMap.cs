﻿using MassTransit.EntityFrameworkIntegration;

namespace Booking.BookService.StateMachine.BookReservation
{
    public class BookReservationMap : SagaClassMapping<BookReservationState>
    {
        // nie jest konieczna mapa, EF wiekszosc zalatwi za nas
    }
}
