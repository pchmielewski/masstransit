﻿using System;
using Automatonymous;
using Booking.BookService.Messages.Events;
using Booking.Contracts.BookService.Commands;
using Booking.Contracts.CustomerService.Events;

namespace Booking.BookService.StateMachine.BookReservation
{
    public class BookReservationStateMachine : MassTransitStateMachine<BookReservationState>
    {
        //states
        public State Interested { get; private set; }
        public State OfferSended { get; private set; }

        //commands
        public Event<ICustomerConsiderReservationCommand> CustomerConsiderReservation { get; private set; }
        public Event<ISendOfferCommand> SendOffer { get; private set; }

        //events
        public Event<ICustomerRegisteredEvent> CustomerRegistered { get; private set; }

        public BookReservationStateMachine()
        {
            InstanceState(s => s.CurrentState);

            Event(() => CustomerConsiderReservation, x => x.CorrelateById(context => context.Message.CorrelationId)); // musi byc correlationId poniewaz dany uzytkownik moze 

            Initially(
                
                When(CustomerConsiderReservation)
                    .Then(HandleMapCustomerConsiderReservation()) 
                    .ThenAsync(context => Console.Out.WriteLineAsync($"Klient {context.Data.CustomerId} przegladal oferte {context.Data.ServiceName}"))
                    .TransitionTo(Interested)
                    .Publish(context => new IntrestedCustomerEvent
                    {
                        CorrelationId = context.Data.CorrelationId,
                        CustomerId = context.Data.CustomerId,
                        End = context.Data.End,
                        Start = context.Data.Start,
                        ServiceId = context.Data.ServiceId,
                        ServiceName = context.Data.ServiceName,
                        ServiceProviderId = context.Data.ServiceProviderId,
                    })
            );

            During(Interested,

                 When(SendOffer)
                    .ThenAsync(context => Console.Out.WriteLineAsync($"Wiadomosc dla klienta {context.Instance.CustomerId} <powinna zostac jakosc dodana>"))
                    .ThenAsync(context => Console.Out.WriteLineAsync($".....<Tutaj trzeba zaimplementowac Communication Service>......"))
                    .TransitionTo(OfferSended)
            );
        }

        private static Action<BehaviorContext<BookReservationState, ICustomerConsiderReservationCommand>> HandleMapCustomerConsiderReservation()
        {
            return context =>
            {
                context.Instance.CorrelationId = context.Data.CorrelationId;
                context.Instance.CustomerId = context.Data.CorrelationId;
                context.Instance.End = context.Data.End;
                context.Instance.Start = context.Data.Start;
                context.Instance.ServiceId = context.Data.ServiceId;
                context.Instance.ServiceName = context.Data.ServiceName;
                context.Instance.ServiceProviderId = context.Data.ServiceProviderId;
            };
        }
    }
}
