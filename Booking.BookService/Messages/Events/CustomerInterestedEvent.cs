﻿using System;
using Booking.Contracts.BookService.Events;

namespace Booking.BookService.Messages.Events
{
    public class CustomerInterestedEvent : ICustomerInterestedEvent
    {
        public Guid CorrelationId { get; set; }
        public DateTime Timestamp { get; set; }
        public Guid CustomerId { get; set; }
        public Guid ServiceProviderId { get; set; }
        public Guid ServiceId { get; set; }
        public string ServiceName { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
    }
}
