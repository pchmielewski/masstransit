﻿using System;
using Booking.Contracts.BookService.Events;

namespace Booking.BookService.Messages.Events
{
    public class ReservationVisitedEvent : IReservationVisitedEvent
    {
        public Guid ServiceId { get; set; }
        public int Count { get; set; }
    }
}
