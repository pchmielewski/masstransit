﻿using System;
using System.Configuration;
using Booking.Contracts.Configuration;
using MassTransit;
using Automatonymous;
using Booking.BookService.Consumers;
using Booking.BookService.StateMachine.BookReservation;
using MassTransit.EntityFrameworkIntegration;
using MassTransit.EntityFrameworkIntegration.Saga;
using MassTransit.Saga;

namespace Booking.BookService
{
    class Program
    {
        private static IBusControl _bus;

        static void Main(string[] args)
        {
            Console.Title = "Book Service";

            _bus = Bus.Factory.CreateUsingRabbitMq(cfg =>
            {

                var bookingReservationStateMachine = new BookReservationStateMachine();

                var host = cfg.Host(new Uri(RabbitMqConstants.RabbitMqUri), h =>
                {
                    h.Username(RabbitMqConstants.UserName);
                    h.Password(RabbitMqConstants.Password);
                });

                cfg.ReceiveEndpoint(host, RabbitMqConstants.BookService_BookReservation, e =>
                {
                    e.StateMachineSaga(bookingReservationStateMachine, SagaRepository().Value);
                    e.PrefetchCount = 8;
                });

                cfg.ReceiveEndpoint(host, RabbitMqConstants.InterestedCustomerQueue, e =>
                {
                    // do transakcji uzywac TransactionScope
                    // co do transakcji pomiedzy consumerami, nie jestem do tego przekonany zabardzo 
                    // wydaje mi sie ze powinno sie wykonywac operacje na state machine 
                    // po zawsze przetrzymujemy ostatni stan
                    /*e.UseTransaction(configurator =>
                    {
                        configurator.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted;
                        configurator.Timeout = TimeSpan.FromSeconds(90);
                    });*/

                    e.Consumer<IntrestedCustomerConsumer>();
                });

                cfg.ReceiveEndpoint(host, RabbitMqConstants.CustomerVisitedQueue, e =>
                {
                    e.Consumer<CustomerVisitedConsumer>();
                });



                // narazie nir rozumiem
                //var builder = new RoutingSlipBuilder(NewId.NextGuid());
                //builder.AddActivity("LogInterestedCustomer", new Uri(RabbitMqConstants.RabbitMqUri+ "execute_log_interested_customer"));
                //builder.Build();

                //builder.AddSubscription(new Uri(""), RoutingSlipEvents.All);
            });

            _bus.Start();

            Console.WriteLine("Book Service is running");


            Console.ReadLine();
            _bus?.Stop();
        }


        private static Lazy<ISagaRepository<BookReservationState>> SagaRepository()
        {

            SagaDbContextFactory sagaDbContextFactory =
                () =>
                    new SagaDbContext<BookReservationState, BookReservationMap>(
                        ConfigurationManager.ConnectionStrings["BookService"].ConnectionString);


            return new Lazy<ISagaRepository<BookReservationState>>(
                () => new EntityFrameworkSagaRepository<BookReservationState>(sagaDbContextFactory));
            
        }
    }
}
