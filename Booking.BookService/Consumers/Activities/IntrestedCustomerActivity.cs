﻿using System;
using System.Threading.Tasks;
using Automatonymous;
using Booking.Contracts.BookService.Events;
using GreenPipes;
using MassTransit.Logging;

namespace Booking.BookService.Consumers.Activities
{
    // do testowania 
    public class IntrestedCustomerActivity : Activity<IIntrestedCustomerEvent, ILog>
    {
        public void Probe(ProbeContext context)
        {
            // nie wiem jak dziala
        }

        public void Accept(StateMachineVisitor visitor)
        {
            // moe wiem jak dziala, chyuba przesyla do innej aktywnosci 
        }

        public async Task Execute(BehaviorContext<IIntrestedCustomerEvent, ILog> context, Behavior<IIntrestedCustomerEvent, ILog> next)
        {
            // robi chyba to co consumer
            await Console.Out.WriteLineAsync($"##### Exception: " + context.Instance.CustomerId);
        }

        public async Task Faulted<TException>(BehaviorExceptionContext<IIntrestedCustomerEvent, ILog, TException> context, Behavior<IIntrestedCustomerEvent, ILog> next)
            where TException : Exception
        {
            // lapie bledy 
            await Console.Out.WriteLineAsync($"##### Exception: " + context.Exception.Message);
        }
    }
}
