﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using Booking.BookService.DAL;
using Booking.BookService.DAL.Entities;
using Booking.BookService.Messages.Events;
using Booking.Contracts.BookService.Commands;
using Booking.Contracts.BookService.Events;
using MassTransit;

namespace Booking.BookService.Consumers
{
    public class CustomerVisitedConsumer : IConsumer<ICustomerVisitReservationCommand>
    {
        public async Task Consume(ConsumeContext<ICustomerVisitReservationCommand> context)
        {
            await Console.Out.WriteLineAsync($"Klient odwiedzil {context.Message.ServiceName} w dniu {context.Message.Start} - {context.Message.End}");

            var mes = context.Message;
            using (var dbContext = new BookingContext())
            {
                var visited =
                    await dbContext.CustomerVisited.
                        FirstOrDefaultAsync(x => x.ServiceId == mes.ServiceId);

                //nie powinno tak byc ze toList, bo ciagnie wtedy wszystko
                if (visited == null)
                {
                    dbContext.CustomerVisited.Add(new CustomerVisited
                    {
                        End = mes.End,
                        ServiceId = mes.ServiceId,
                        Start = mes.Start,
                        ServiceName = mes.ServiceName,
                        ServiceProviderId = mes.ServiceProviderId,
                        Count = 1
                    });
                    dbContext.SaveChanges();

                    await Console.Out.WriteLineAsync($"Dodalem");

                }
                else
                {
                    visited.Count = visited.Count + 1;
                    dbContext.SaveChanges();
                    await Console.Out.WriteLineAsync($"Zwiekszylem");
                }
            }

            using (var dbContext = new BookingContext())
            {
                var visited = dbContext.CustomerVisited.ToList().FirstOrDefault(x => x.Start.ToShortDateString() == mes.Start.ToShortDateString() && x.End.ToShortDateString() == mes.End.ToShortDateString() && x.ServiceId == mes.ServiceId);
                if (visited != null && visited.Count >3)
                {
                    await context.Publish<IReservationVisitedEvent>(new ReservationVisitedEvent
                    {
                        ServiceId = visited.ServiceId,
                        Count = visited.Count
                    });

                }
            }
        }
    }
}
