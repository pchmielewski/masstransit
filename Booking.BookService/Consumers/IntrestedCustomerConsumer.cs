﻿using System;
using System.Threading.Tasks;
using Booking.BookService.DAL;
using Booking.BookService.DAL.Entities;
using Booking.BookService.Messages.Events;
using Booking.Contracts.BookService.Events;
using MassTransit;

namespace Booking.BookService.Consumers
{
    public class IntrestedCustomerConsumer : IConsumer<IIntrestedCustomerEvent>
    {
        public async Task Consume(ConsumeContext<IIntrestedCustomerEvent> context)
        {
            using (var dbContext = new BookingContext())
            {
                dbContext.InterestedCustomers.Add(new InterestedCustomer
                {
                    CustomerId = context.Message.CustomerId,
                    Start = context.Message.Start,
                    End = context.Message.End,
                    ServiceId = context.Message.ServiceId,
                    ServiceName = context.Message.ServiceName,
                    ServiceProviderId = context.Message.ServiceProviderId,
                });

                await dbContext.SaveChangesAsync();

                await Console.Out.WriteLineAsync($"Klient {context.Message.CustomerId} zainteresowany {context.Message.ServiceName} usługą został dodany");
            }

            await context.RespondAsync<ICustomerInterestedEvent>(new CustomerInterestedEvent
            {
                // sprawdzic context.CorellarionId
                CorrelationId = context.Message.CorrelationId,
                CustomerId = context.Message.CustomerId,
                Start = context.Message.Start,
                End = context.Message.End,
                ServiceId = context.Message.ServiceId,
                ServiceName = context.Message.ServiceName,
                ServiceProviderId = context.Message.ServiceProviderId,
            });
        }
    }
}
