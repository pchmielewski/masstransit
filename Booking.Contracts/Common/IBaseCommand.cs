﻿using System;
using MassTransit;

namespace Booking.Contracts.Common
{
    public interface IBaseCommand : CorrelatedBy<Guid>
    {
        /// <summary>
        /// The time when the command was sent
        /// </summary>
        DateTime Timestamp { get; }
    }
}
