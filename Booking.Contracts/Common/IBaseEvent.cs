﻿using System;
using MassTransit;

namespace Booking.Contracts.Common
{
    public interface IBaseEvent : CorrelatedBy<Guid>
    {
        //CorrelationID - dziala tutaj jako eventID

        /// <summary>
        /// The timestamp when the event was produced
        /// </summary>
        DateTime Timestamp { get; }
    }
}
