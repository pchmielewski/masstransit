﻿namespace Booking.Contracts.Configuration
{
    public static class RabbitMqConstants
    {
        public const string RabbitMqUri = "rabbitmq://localhost/booking/";
        public const string UserName = "guest";
        public const string Password = "guest";
        public const string CustomerService = "customer.service";
        public const string BookService = "book.service";
        public const string RegisterCustomerSaga = ".register";

        public const string BookService_BookReservation = "book.service.bookReservation";

        public const string ServiceProviderClient = "serviceprovider.client";

        public const string InterestedCustomerQueue = "interested_customer";

        public const string CustomerVisitedQueue = "visited_customer";
    }
}
