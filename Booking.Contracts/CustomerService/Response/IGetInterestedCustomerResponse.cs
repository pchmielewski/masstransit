﻿using System;

namespace Booking.Contracts.CustomerService.Response
{
    public interface IGetInterestedCustomerResponse
    {
        Guid CustomerId { get; set; }
        string FirstName { get; set; }
        string Lastname { get; set; }
        DateTime BirthDay { get; set; }
        string City { get; set; }
        string Country { get; set; }
    }
}
