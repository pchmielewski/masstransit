﻿using System;
using Booking.Contracts.Common;

namespace Booking.Contracts.CustomerService.Events
{
    public interface ICustomerReceivedEvent : IBaseEvent
    {
        Guid CustomerId { get; set; }
        string FirstName { get; set; }
        string Lastname { get; set; }
        DateTime BirthDay { get; set; }
        string City { get; set; }
        string Country { get; set; }
    }
}
