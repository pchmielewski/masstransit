﻿using System;

namespace Booking.Contracts.CustomerService.Request
{
    public interface IGetInterestedCustomer
    {
        Guid IntrestedCustomerId { get; set; }
    }
}
