﻿using System;
using Booking.Contracts.Common;

namespace Booking.Contracts.CustomerService.Commands
{
    public interface IUpdateCustomerCommand : IBaseCommand
    {
        Guid CustomerId { get; set; }
        string FirstName { get; set; }
        string Lastname { get; set; }
        DateTime BirthDay { get; set; }
        string City { get; set; }
        string Country { get; set; }
    }
}
