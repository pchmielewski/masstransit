﻿using System;
using Booking.Contracts.Common;

namespace Booking.Contracts.BookService.Events
{
    public interface IIntrestedCustomerEvent : IBaseEvent
    {
        Guid CustomerId { get; set; }
        Guid ServiceProviderId { get; set; }
        Guid ServiceId { get; set; }

        string ServiceName { get; set; }

        DateTime Start { get; set; }
        DateTime End { get; set; }
    }
}
