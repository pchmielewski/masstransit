﻿using System;

namespace Booking.Contracts.BookService.Events
{
    public interface IReservationVisitedEvent
    {
        Guid ServiceId { get; set; }
        int Count { get; set; }
    }
}
