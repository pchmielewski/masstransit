﻿using System;

namespace Booking.Contracts.BookService.Commands
{
    public interface ICustomerVisitReservationCommand
    {
        Guid ServiceProviderId { get; set; }
        Guid ServiceId { get; set; }

        string ServiceName { get; set; }

        DateTime Start { get; set; }
        DateTime End { get; set; }
    }
}
