﻿using System;
using Booking.Contracts.Common;

namespace Booking.Contracts.BookService.Commands
{
    public interface ISendOfferCommand : IBaseCommand
    {
        Guid CustomerId { get; set; }
        Guid ServiceProviderId { get; set; }
        Guid ServiceId { get; set; }

        string ServiceName { get; set; }

        DateTime Start { get; set; }
        DateTime End { get; set; }

        string Message { get; set; }
        decimal Cost { get; set; }
    }
}
