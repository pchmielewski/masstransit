﻿namespace Booking.Contracts.Logging
{
    public interface ILog
    {
        string LogMessage { get; set; }
    }
}
