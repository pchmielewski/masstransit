﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using Booking.CustomerService.DAL.Entities;

namespace Booking.CustomerService.DAL
{
    public class CustomerContext : DbContext 
    {
        public CustomerContext() : base("name=CustomerService")
        {
            
        }

        public DbSet<Customer> Customers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("CustomerService");

            modelBuilder.Entity<Customer>().HasKey(x => x.CustomerId);
            //modelBuilder.Entity<Customer>().Property(x => x.CustomerId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            base.OnModelCreating(modelBuilder);
        }
    }
}
